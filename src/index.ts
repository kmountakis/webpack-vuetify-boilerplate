import moment from 'moment'
import {Vue, Vuetify} from './init'
import './widgets/x-footer'

new Vue({
    el: "#app",
    vuetify: new Vuetify(),
    data: {
        currentDate: moment().format(),
        inputData: ""
    },
    methods: {
        someMethod(x: number) {
            return x.toFixed(2)
        }
    }
})
