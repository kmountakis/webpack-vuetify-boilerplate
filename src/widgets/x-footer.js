import moment from 'moment'
import {Vue} from '../init'

const Footer = {
    template: "#x-footer",
    props: [""],
    data() {
        return {
            today: moment().format("YYYY")
        }
    },
}

Vue.component("x-footer", Footer)